<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

#File
$router->get('/files/{container}/{filename}','FileController@getFile');

#Account
$router->group(['prefix'=>'accounts'], function () use ($router){
    $router->get('','AccountController@getAccounts');
    $router->post('','AccountController@addAccount');
    $router->get('{id}','AccountController@getAccount');
    $router->put('{id}','AccountController@editAccount');
    $router->delete('{id}','AccountController@delAccount');
    $router->post('{id}/image','AccountController@addAccountProfpic');
    $router->post('login','AccountController@login');
});

$router->group(['prefix'=>'events'], function() use ($router){
    $router->get('','EventController@getEvents');
    $router->post('','EventController@addEvent');
    $router->get('{id}','EventController@getEvent');
    $router->put('{id}','EventController@editEvent');
    $router->delete('{id}','EventController@delEvent');
    $router->post('{id}/image','EventController@addEventPreview');
});