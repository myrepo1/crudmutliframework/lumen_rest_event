<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

use App\Models\Event;

class EventFactory extends Factory
{
    protected $model = Event::class;

    public function definition()
    {
        return [
            'name'=>$this->faker->text(30),
            'shortname'=>$this->faker->text(8),
            'start'=>$this->faker->dateTimeBetween('1 days','+20 days'),
            'end'=>$this->faker->dateTimeBetween('$start','10 days'),
            'description'=>$this->faker->text,
            'location'=>$this->faker->address
        ];
    }
}