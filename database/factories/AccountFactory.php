<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

use App\Models\Account;

class AccountFactory extends Factory
{
    protected $model = Account::class;

    public function definition()
    {
        return [
            'username'=>$this->faker->unique()->userName,
            'password'=>Hash::make('test123'),
            'name'=>$this->faker->unique()->name,
            'phone'=>$this->faker->phoneNumber,
            'is_staff'=>true
        ];
    }
}