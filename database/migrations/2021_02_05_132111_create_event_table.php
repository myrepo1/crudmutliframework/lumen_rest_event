<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->id();
            $table->string('name',150)->nullable(false);
            $table->string('shortname',150)->nullable();
            $table->dateTime('start')->nullable(false);
            $table->dateTime('end')->nullable(true);
            $table->text('description')->nullable(true);
            $table->string('location')->nullable(true);
            $table->string('preview',200)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
