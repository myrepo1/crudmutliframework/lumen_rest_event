<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;

use App\Http\Helpers\Response;

class JWTAuth
{
    use Response;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if(preg_match('/^Bearer (.*)$/',$token,$matches)){
            $jwt_token = $matches[1];
        } else {
            return $this->getResponse(401,['message'=>'Token is invalid.']);
        }
        try {
            $result = JWT::decode($jwt_token,env('JWT_SECRET'),['HS256']);
            $request->request->add(['identity'=>(array)$result->identity]);
        } catch (\Throwable $th) {
            if($th instanceof ExpiredException) {
                return $this->getResponse(401,['message'=>'Token is expired.']);
            }
            elseif ($th instanceof BeforeValidException) {
                return $this->getResponse(401,['message'=>'Token is not yet valid.']);
            }

            elseif ($th instanceof SignatureInvalidException) {
                return $this->getResponse(401,['message'=>'Failed to validate token']);
            }
            else {
                return $this->getResponse(401,['data'=>'Failed to parse token']);
            }
        }
        return $next($request);
    }
}
