<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'data_id'=>$this->id,
            'username'=>$this->username,
            'name'=>$this->name,
            'phone'=>$this->phone,
            'is_staff'=>$this->is_staff===null?false:$this->is_staff,
            'profpic'=>$this->profpic
        ];
    }
};
