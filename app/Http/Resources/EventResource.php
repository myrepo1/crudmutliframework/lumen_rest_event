<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'data_id'=>$this->id,
            'name'=>$this->name,
            'shortname'=>$this->shortname,
            'start'=>$this->start->format('d-m-Y H:i'),
            'end'=>$this->end->format('d-m-Y H:i'),
            'description'=>$this->description,
            'location'=>$this->location,
            'preview'=>$this->preview
        ];
    }
};
