<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Event;
use App\Http\Resources\EventResource;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     $this->middleware('auth',[
         'except'=>['getEvents','getEvent']
         ]);
    }

    public function addEvent(Request $request)
    {
        $user = $this->getIndentity($request);
        if(!$user['is_staff']){
            return $this->getResponse(403);
        }
        $raw_data = $request->json()->all();
        $validator = Validator::make($raw_data,[
            'name'=>'required|string|unique:App\Models\Event,name',
            'shortname'=>'string|nullable',
            'start'=>'required|date_format:"d-m-Y H:i"',
            'end'=>'nullable|date_format:"d-m-Y H:i"|after:start',
            'description'=>'string|nullable',
            'location'=>'string|nullable'
        ]);
        if($validator->fails()){
            $detail_message = $this->generateErrorMessage($validator);
            return $this->getResponse(400,['detail_message'=>$detail_message]);
        }
        $data = $validator->validated();
        $event = Event::create($data);
        $result = new EventResource($event);
        return $this->getResponse(201,['data'=>$result]);
    }

    public function getEvents()
    {
        $result = EventResource::collection(Event::all());
        return $this->getResponse(200,['data'=>$result]);
    }

    public function getEvent($id)
    {
        $event = Event::find($id);
        if(!$event){
            return $this->getResponse(404);
        }
        $result = new EventResource($event);
        return $this->getResponse(200,['data'=>$result]);
    }

    public function editEvent(Request $request, $id)
    {
        $user = $this->getIndentity($request);
        if(!$user['is_staff']){
            return $this->getResponse(403);
        }
        $event = Event::find($id);
        if(!$event){
            return $this->getResponse(404);
        }
        $raw_data = $request->json()->all();
        $validator = Validator::make($raw_data,[
            'name'=>'sometimes|string|required|unique:App\Models\Event,name',
            'shortname'=>'string|nullable',
            'start'=>'sometimes|required|date_format:"d-m-Y H:i"',
            'end'=>'sometimes|nullable|date_format:"d-m-Y H:i"|after:start',
            'description'=>'string|nullable',
            'location'=>'string|nullable'
        ]);
        if($validator->fails()){
            $detail_message = $this->generateErrorMessage($validator);
            return $this->getResponse(400,['detail_message'=>$detail_message]);
        }
        $data = $validator->validated();
        if(array_key_exists('start',$data)||array_key_exists('end',$data)){
            $start_val = $data['start']??$event->start;
            $end_val = $data['end']??$event->end;
            if ($end_val&&$start_val>$end_val){
                $this->addErrorMessage(null,'The end must be a date after start.',$validator);
            }
        }
        if($validator->fails()){
            $detail_message = $this->generateErrorMessage($validator);
            return $this->getResponse(400,['detail_message'=>$detail_message]);
        }
        foreach ($data as $key => $value) {
            $event->setAttribute($key,$value);
        }
        $event->save();
        $result = new EventResource($event);
        return $this->getResponse(200,['data'=>$result]);
    }

    public function delEvent(Request $request, $id)
    {
        $user = $this->getIndentity($request);
        if(!$user['is_staff']){
            return $this->getResponse(403);
        }
        $event = Event::find($id);
        if(!$event){
            return $this->getResponse(404);
        }
        $event->delete();
        return $this->getResponse(200);
    }

    public function addEventPreview(Request $request, $id)
    {
        $user = $this->getIndentity($request);
        if(!$user['is_staff']){
            return $this->getResponse(403);
        }
        $event = Event::find($id);
        if(!$event){
            return $this->getResponse(404);
        }
        if(!$request->file('preview')){
            return $this->getResponse(400,['message'=>'File not found.']);
        }
        if($event->preview){
            $this->deleteFile($event->preview);
        }
        $event->preview = $this->saveFile($request,'preview');
        $event->save();
        $result = new EventResource($event);
        return $this->getResponse(200,['data'=>$result]);
    }
}