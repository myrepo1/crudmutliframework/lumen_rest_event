<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;

class FileController extends Controller {
    public function __construct()
    {
        
    }

    public function getFile($container,$filename)
    {
        $filename = implode('.',explode('__',$filename));
        if(env('APP_ENV')==='testing'){
            $dir = 'test/'.$container;
        } else {
            $dir = 'app/'.$container;
        }
        $path = storage_path($dir).'/'.$filename;
        if(!File::exists($path)) {
            return $this->getResponse(404);
        }
        $file = file_get_contents($path);
        return response($file,200,['content-type'=>'image/jpeg']);
    }
}