<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Helpers\Response;

class Controller extends BaseController
{
    use Response;

    public function generateErrorMessage($validator)
    {
        return $validator->errors()->toArray();
    }

    public function addErrorMessage($field,$message,$validator)
    {
        if($field===null){
            $field = '__all__';
        }
        $validator->getMessageBag()->add($field,$message);
    }

    protected function getIndentity($request)
    {
        return $request->request->get('identity');
    }

    public function saveFile($request,$dir)
    {
        $ext = $request->file($dir)->getClientOriginalExtension();
        $filename = Str::random();
        $ori_filename = $filename.'.'.$ext;
        $url_filename = $filename.'__'.$ext;
        if(env('APP_ENV')==='testing'){
            $dest = 'test/'.$dir;
        } else {
            $dest = 'app/'.$dir;
        }
        $request->file($dir)->move(storage_path($dest),$ori_filename);
        return $request->root().'/files'.'/'.$dir.'/'.$url_filename;
    }

    public function deleteFile($url)
    {
        $splited_url = array_reverse(explode('/',$url));
        $filename = $splited_url[0];
        $container = $splited_url[1];
        if(env('APP_ENV')==='testing'){
            $dir = 'test/'.$container;
        } else {
            $dir = 'app/'.$container;
        }
        $path = storage_path($dir).'/'.$filename;
        if(File::exists($path)) {
            File::delete($path);
        }
    }
}
