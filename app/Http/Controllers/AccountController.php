<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\Account;
use App\Http\Helpers\Security;
use App\Http\Resources\AccountResource;

class AccountController extends Controller {
    use Security;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     $this->middleware('auth',[
         'except'=>['login','addAccount']
         ]);
    }

    public function addAccount(Request $request)
    {
            $raw_data = $request->json()->all();
            $validator = Validator::make($raw_data,[
                'username'=>'string|required|unique:App\Models\Account,username',
                'password'=>'string|required',
                'name'=>'string|required',
                'phone'=>'string|numeric|nullable',
                'is_profpic'=>'boolean'
            ]);
            if($validator->fails()){
                    $detail_message = $this->generateErrorMessage($validator);
                    return $this->getResponse(400,['detail_message'=>$detail_message]);
            }
            $data = $validator->validated();
            $data['password'] = Hash::make($data['password']);
            $account = Account::create($data);
            $result = new AccountResource($account);
            return $this->getResponse(201,['data'=>$result]);
    }

    public function getAccounts(Request $request)
    {
        $user = $this->getIndentity($request);
        if(!$user['is_staff']) {
            return $this->getResponse(403);
        }
        $result = AccountResource::collection(Account::all());
        return $this->getResponse('m200',['data'=>$result]);
    }

    public function getAccount(Request $request,$id)
    {
        $user = $this->getIndentity($request);
        if($user['id']!=$id&&!$user['is_staff']){
            return $this->getResponse(403);
        }
        $account = Account::find($id);
        if(!$account) {
            return $this->getResponse(404);
        }
        $result = new AccountResource($account);
        return $this->getResponse(200,['data'=>$result]);
    }

    public function editAccount(Request $request, $id)
    {
        $user = $this->getIndentity($request);
        if($user['id']!=$id&&!$user['is_staff']){
            return $this->getResponse(403);
        }
        $account = Account::find($id);
        if(!$account) {
            return $this->getResponse(404);
        }
        $raw_data = $request->json()->all();
        $validator = Validator::make($raw_data,[
            'username'=>'sometimes|required|string|unique:App\Models\Account,username,'.$id,
            'password'=>'sometimes|string|required',
            'name'=>'sometimes|string|required',
            'phone'=>'string|numeric|nullable',
            'is_profpic'=>'boolean'
        ]);
        if($validator->fails()){
            $detail_message = $this->generateErrorMessage($validator);
            return $this->getResponse(400,['detail_message'=>$detail_message]);
        }
        $data = $validator->validated();
        if(array_key_exists('password',$data)) {
            $data['password'] = Hash::make($data['password']);
        }
        foreach ($data as $key => $value) {
            $account->setAttribute($key,$value);
        }
        $account->save();
        $result = new AccountResource($account);
        return $this->getResponse(200,['data'=>$result]);
    }

    public function delAccount(Request $request, $id)
    {
        $user = $this->getIndentity($request);
        if($user['id']!=$id&&!$user['is_staff']){
            return $this->getResponse(403);
        }
        $account = Account::find($id);
        if(!$account) {
            return $this->getResponse(404);
        }
        $account->delete();
        return $this->getResponse(200);
    }

    public function addAccountProfpic(Request $request, $id)
    {
        $user = $this->getIndentity($request);
        if($user['id']!=$id&&!$user['is_staff']){
            return $this->getResponse(403);
        }
        $account = Account::find($id);
        if(!$account) {
            return $this->getResponse(404);
        }
        if(!$request->file('profpic')){
            return $this->getResponse(400,['message'=>'File not found.']);
        }
        if($account->profpic){
            $this->deleteFile($account->profpic);
        }
        $account->profpic = $this->saveFile($request,'profpic');
        $account->save();
        $result = new AccountResource($account);
        return $this->getResponse(200,['data'=>$result]);
    }

    public function login(Request $request)
    {
        $raw_data = $request->json()->all();
        $validator = Validator::make($raw_data,[
            'username'=>'string|required',
            'password'=>'string|required'
        ]);
        if($validator->fails()){
            $detail_message = $this->generateErrorMessage($validator);
            return $this->getResponse(400,['detail_message'=>$detail_message]);
        }
        $data = $validator->validated();
        $account = Account::where('username',$data['username'])->first();
        if(!$account || !Hash::check($data['password'],$account->password)) {
            $validator->getMessageBag()->add('__all__','Wrong username or password.');
            $detail_message = $this->generateErrorMessage($validator);
            return $this->getResponse(400,['detail_message'=>$detail_message]);
        }
        $token = $this->generateJWT(['id'=>$account->id,'is_staff'=>$account->is_staff]);
        $response = ['access_token'=>$token,'id'=>$account->id,'is_staff'=>$account->is_staff];
        return $this->getResponse(200,['data'=>$response]);
    }
}