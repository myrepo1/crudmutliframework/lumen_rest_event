<?php

namespace App\Http\Helpers;

trait Response
{
    protected $responses = [
        '200'=>[
            'status_code'=>200,
            'status'=>true,
            'message'=>'Request Success.',
            'data'=>null
        ],
        'm200'=>[
            'status_code'=>200,
            'status'=>true,
            'message'=>'Request Success.',
            'data'=>[]
        ],
        '201'=>[
            'status_code'=>201,
            'status'=>true,
            'message'=>'Request Created.',
            'data'=>null
        ],
        '400'=>[
            'status_code'=>400,
            'status'=>false,
            'message'=>'Validation Failed.',
            'data'=>null
        ],
        '401'=>[
            'status_code'=>401,
            'status'=>false,
            'message'=>'Unauthenticated Request.',
            'data'=>null
        ],
        '403'=>[
            'status_code'=>403,
            'status'=>false,
            'message'=>'Forbidden Request.',
            'data'=>null
        ],
        '404'=>[
            'status_code'=>404,
            'status'=>false,
            'message'=>'Resource not Found.',
            'data'=>null
        ],
        '500'=>[
            'status_code'=>500,
            'status'=>false,
            'message'=>'Internal Server Error.',
            'data'=>null
        ]
    ];

    public function getResponse($code,$params=[]) {
        $initial_response = $this->responses[$code];
        $response = array_merge($initial_response,$params);
        return response()->json($response)->setStatusCode($response['status_code']);
    }
}