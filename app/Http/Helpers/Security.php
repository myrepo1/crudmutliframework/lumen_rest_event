<?php

namespace App\Http\Helpers;
use Carbon\Carbon;
use Firebase\JWT\JWT;

trait Security
{
    public function generateJWT($data,$unit='day',$expired=1)
    {
        $time = Carbon::now()->add($unit,$expired);
        $payload = [
            'identity'=>$data,
            'exp'=>$time->unix()
        ];
        return JWT::encode($payload,env('JWT_SECRET'));
    }
}