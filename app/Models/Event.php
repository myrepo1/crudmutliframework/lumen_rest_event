<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $guarded = ['preview'];
    protected $fillable = ['name','shortname','start','end','description','location'];
    protected $casts = [
        'start'=>'datetime:d-m-Y H:i',
        'end'=>'datetime:d-m-Y H:i'
    ];
}