<?php

class EventDeleteTest extends TestCase
{
    public function testSuccessDeleteEvent()
    {
        $url = '/events/'.$this->getEvent()->id;
        $this->jsonUseJWT('DELETE',$url);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true
        ]);
    }

    public function testFailedDeleteEventNoToken()
    {
        $url = '/events/'.$this->getEvent()->id;
        $this->json('DELETE',$url);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false
        ]);
    }

    public function testFailedDeleteEventUseNonStaff()
    {
        $this->addAccount(['is_staff'=>false]);
        $url = '/events/'.$this->getEvent()->id;
        $this->jsonUseJWT('DELETE',$url,[],[],1);
        $this->seeStatusCode(403);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
        ]);
        $this->seeJsonContains([
            'status_code'=>403,
            'status'=>false
        ]);
    }

    public function testFailedDeleteEventNotFound()
    {
        $url = '/events/'.strval($this->getEvent()->id+1);
        $this->jsonUseJWT('DELETE',$url);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false
        ]);        
    }
}