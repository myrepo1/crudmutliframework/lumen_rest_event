<?php

class EventGetPreviewTest extends TestCase
{
    public function testSuccessEventGetPreview(){
        $this->addEvent([],true);
        $url = $this->getEvent(1)->preview;
        $this->get($url)->seeStatusCode(200);
    }

    public function testFailedEventGetPreviewNotFound(){
        $this->addEvent([],true);
        $url = $this->getEvent(1)->preview.'a';
        $this->get($url)->seeStatusCode(404);
    }
}