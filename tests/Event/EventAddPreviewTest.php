<?php

class EventAddPreviewTest extends TestCase
{
    public function testSuccessEventAddPreview()
    {
        $url = '/events/'.$this->getEvent()->id.'/image';
        $file = $this->getRandomImage();
        $this->uploadFileUseJWT($url,['preview'=>$file]);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'name',
                'shortname',
                'start',
                'end',
                'description',
                'location',
                'preview'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true
        ]);
        $this->assertNotNull($this->response['data']['preview']);
        $this->assertTrue($this->checkURL($this->response['data']['preview']));
    }

    public function testFailedAddEventPreviewNoToken()
    {
        $url = '/events/'.$this->getEvent()->id.'/image';
        $file = $this->getRandomImage();
        $this->uploadFile($url,['preview'=>$file]);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false
        ]);
    }

    public function testFailedAddEventPreviewUseNonStaff()
    {
        $this->addAccount(['is_staff'=>false]);
        $url = '/events/'.$this->getEvent()->id.'/image';
        $file = $this->getRandomImage();
        $this->uploadFileUseJWT($url,['preview'=>$file],[],[],'POST',1);
        $this->seeStatusCode(403);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>403,
            'status'=>false
        ]);
    }

    public function testFailedAddEventPreviewEventNotFound()
    {
        $url = '/events/'.strval($this->getEvent()->id+1).'/image';
        $file = $this->getRandomImage();
        $this->uploadFileUseJWT($url,['preview'=>$file]);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false
        ]);        
    }
}