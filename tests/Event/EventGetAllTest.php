<?php

class EventGetAllTest extends TestCase
{
    public function testSuccessEventGetAll()
    {
        $this->jsonUseJWT('GET','/events');
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>['*'=>[
                'data_id',
                'name',
                'start',
                'end',
                'description',
                'location',
                'preview'
            ]]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true
        ]);
        $this->seeJson(['name'=>$this->getEvent()->name]);
    }
}