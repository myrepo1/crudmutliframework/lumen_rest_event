<?php


class EventCreateTest extends TestCase
{
    public function testSuccessCreateEvent()
    {
        $start = new DateTime();
        $end = new DateTime() ;
        $start = $start->modify('+1 day')->format('d-m-Y H:i');
        $end = $end->modify('+2 day')->format('d-m-Y H:i');
        $payload = [
            'name'=>'Test Event',
            'shortname'=>'TE1',
            'start'=>$start,
            'end'=>$end,
            'description'=>'This is test event',
            'location'=>'earth'
        ];
        $this->jsonUseJWT('POST','/events',$payload);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'name',
                'shortname',
                'start',
                'end',
                'description',
                'location',
                'preview'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>201,
            'status'=>true,
            'name'=>$payload['name'],
            'shortname'=>$payload['shortname'],
            'start'=>$payload['start'],
            'end'=>$payload['end'],
            'description'=>$payload['description'],
            'location'=>$payload['location'],
            'preview'=>null
        ]);
    }

    public function testFailedCreateEventNoToken()
    {
        $start = new DateTime();
        $end = new DateTime() ;
        $start = $start->modify('+1 day')->format('d-m-Y H:i');
        $end = $end->modify('+2 day')->format('d-m-Y H:i');
        $payload = [
            'name'=>'Test Event',
            'shortname'=>'TE1',
            'start'=>$start,
            'end'=>$end,
            'description'=>'This is test event',
            'location'=>'earth'
        ];
        $this->json('POST','/events',$payload);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false
        ]);
    }

    public function testFailedCreateEventUseNonStaff()
    {
        $start = new DateTime();
        $end = new DateTime() ;
        $start = $start->modify('+1 day')->format('d-m-Y H:i');
        $end = $end->modify('+2 day')->format('d-m-Y H:i');
        $payload = [
            'name'=>'Test Event',
            'shortname'=>'TE1',
            'start'=>$start,
            'end'=>$end,
            'description'=>'This is test event',
            'location'=>'earth'
        ];
        $this->addAccount(['is_staff'=>false]);
        $this->jsonUseJWT('POST','/events',$payload,[],1);
        $this->seeStatusCode(403);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>403,
            'status'=>false
        ]);
    }

    public function testFailedCreateEventEmptyField()
    {
        $payload = [
            'name'=>'',
            'shortname'=>'',
            'start'=>null,
            'end'=>null,
        ];

        $this->jsonUseJWT('POST','/events',$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'detail_message',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'name'=>['The name field is required.'],
                'start'=>['The start field is required.']
            ]
        ]);
    }

    public function testFailedCreateEventInvalidValue()
    {
        $payload = [
            'name'=>'Test Event',
            'shortname'=>'TE1',
            'start'=>'asd',
            'end'=>'asd',
            'description'=>'This is test event',
            'location'=>'earth'
        ];
        $this->jsonUseJWT('POST','/events',$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'detail_message',
            'message',
            'data'
        ]);
        $this->assertEquals(400,$this->response['status_code']);
        $this->assertFalse($this->response['status']);
        $this->assertContains('The start does not match the format d-m-Y H:i.',$this->response['detail_message']['start']);
        $this->assertContains('The end does not match the format d-m-Y H:i.',$this->response['detail_message']['end']);
    }

    public function testFailedCreateEventAlreadyExist()
    {
        $start = new DateTime();
        $end = new DateTime() ;
        $start = $start->modify('+1 day')->format('d-m-Y H:i');
        $end = $end->modify('+2 day')->format('d-m-Y H:i');
        $payload = [
            'name'=>$this->getEvent()->name,
            'shortname'=>'TE1',
            'start'=>$start,
            'end'=>$end,
            'description'=>'This is test event',
            'location'=>'earth'
        ];
        $this->jsonUseJWT('POST','/events',$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'detail_message',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'name'=>['The name has already been taken.']
            ]
        ]);
    }

    public function testFailedCreateEventStartAfterEnd()
    {
        $start = new DateTime();
        $end = new DateTime() ;
        $start = $start->modify('+1 day')->format('d-m-Y H:i');
        $end = $end->modify('+2 day')->format('d-m-Y H:i');
        $payload = [
            'name'=>'Test Event',
            'shortname'=>'TE1',
            'start'=>$end,
            'end'=>$start,
            'description'=>'This is test event',
            'location'=>'earth'
        ];
        $this->jsonUseJWT('POST','/events',$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'detail_message',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'end'=>['The end must be a date after start.']
            ]
        ]);
    }
}