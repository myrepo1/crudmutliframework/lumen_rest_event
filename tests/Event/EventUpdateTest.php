<?php

class EventUpdateTest extends TestCase
{
    public function testSuccessEventUpdate()
    {
        $url = '/events/'.$this->getEvent()->id;
        $payload = [
            'name'=>'Changed'
        ];
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'data_id',
                'name',
                'shortname',
                'start',
                'end',
                'description',
                'location',
                'preview'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true,
            'name'=>$payload['name']
        ]);
    }

    public function testFailedEventUpdateNoToken()
    {
        $url = '/events/'.$this->getEvent()->id;
        $payload = [
            'name'=>'Changed'
        ];
        $this->json('PUT',$url,$payload);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false,
            'data'=>null
        ]);
    }

    public function testFailedEventUpdateUseNonStaff()
    {
        $url = '/events/'.$this->getEvent()->id;
        $payload = [
            'name'=>'Changed'
        ];
        $this->addAccount(['is_staff'=>false]);
        $this->jsonUseJWT('PUT',$url,$payload,[],1);
        $this->seeStatusCode(403);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>403,
            'status'=>false,
            'data'=>null
        ]);
    }

    public function testFailedEventUpdateNotFound()
    {
        $url = '/events/'.strval($this->getEvent()->id+1);
        $payload = [
            'name'=>'Changed'
        ];
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false,
            'data'=>null
        ]);
    }

    public function testFailedEventUpdateEmptyField()
    {
        $url = '/events/'.$this->getEvent()->id;
        $payload = [
            'name'=>'',
            'start'=>null
        ];
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'name'=>['The name field is required.'],
                'start'=>['The start field is required.']
            ],
            'data'=>null
        ]);
    }

    public function testFailedEventUpdateInvalidValue()
    {
        $url = '/events/'.$this->getEvent()->id;
        $payload = [
            'start'=>'asdasd',
            'end'=>'asd'
        ];
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message',
            'data'
        ]);
        $this->assertEquals(400,$this->response['status_code']);
        $this->assertFalse($this->response['status']);
        $this->assertContains('The start does not match the format d-m-Y H:i.',$this->response['detail_message']['start']);
        $this->assertContains('The end does not match the format d-m-Y H:i.',$this->response['detail_message']['end']);
    }

    public function testFailedEventUpdateAlreadyExist()
    {
        $this->addEvent([]);
        $url = '/events/'.$this->getEvent()->id;
        $payload = [
            'name'=>$this->getEvent(1)->name,
        ];
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'detail_message',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'name'=>['The name has already been taken.']
            ]
        ]);
    }

    public function testFailedEventUpdateStartAfterEnd()
    {
        $url = '/events/'.$this->getEvent()->id;
        $start = new DateTime();
        $end = new DateTime() ;
        $start = $start->modify('+1 day')->format('d-m-Y H:i');
        $end = $end->modify('+2 day')->format('d-m-Y H:i');
        $payload = [
            'start'=>$end,
            'end'=>$start
        ];
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'detail_message',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'end'=>['The end must be a date after start.']
            ]
        ]);
    }
}