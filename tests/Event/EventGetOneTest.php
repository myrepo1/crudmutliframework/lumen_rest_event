<?php

class EventGetOneTest extends TestCase
{
    public function testSuccessEventGetOne()
    {
        $this->jsonUseJWT('GET','/events/'.$this->getEvent()->id);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'data_id',
                'name',
                'start',
                'end',
                'description',
                'location',
                'preview'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true,
            'name'=>$this->getEvent()->name
        ]);
        $this->assertStringMatchesFormat('%d-%d-%d %d:%d',$this->response['data']['start']);
        $this->assertStringMatchesFormat('%d-%d-%d %d:%d',$this->response['data']['end']);
    }

    public function testFailedEventGetOneNotFound()
    {
        $this->jsonUseJWT('GET','/events/'.strval($this->getEvent()->id+1));
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false,
            'data'=>null
        ]);       
    }
}