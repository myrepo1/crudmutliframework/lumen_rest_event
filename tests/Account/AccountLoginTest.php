<?php

class AccountLoginTest extends TestCase
{
    public function testSuccessLoginAccount()
    {
        $payload = [
            'username'=>$this->getAccount()->username,
            'password'=>'test123'
        ];
        $url = '/accounts/login';
        $this->json('POST',$url,$payload);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'id',
                'is_staff',
                'access_token'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true,
            'id'=>$this->getAccount()->id
        ]);
        $this->assertNotNull($this->response['data']['access_token']);
    }

    public function testFailedAccountLoginEmptyField()
    {
        $payload = [
            'username'=>'',
            'password'=>''
        ];
        $url = '/accounts/login';
        $this->json('POST',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                'username',
                'password'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'username'=>['The username field is required.'],
                'password'=>['The password field is required.']
            ],
            'data'=>null
        ]);
    }

    public function testFailedAccountLoginWrongPayload()
    {
        $payload = [
            'username'=>'asdasd',
            'password'=>'asdasd'
        ];
        $url = '/accounts/login';
        $this->json('POST',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                '__all__'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                '__all__'=>['Wrong username or password.']
            ],
            'data'=>null
        ]);
    }
}