<?php

class AccountUpdateTest extends TestCase
{
    public function testSuccessUpdateAccount()
    {
        $payload = [
            'name'=>'Update',
            'phone'=>'54321'
        ];
        $url = '/accounts/'.$this->getAccount()->id;
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'data_id',
                'username',
                'name',
                'phone',
                'profpic',
                'is_staff'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true,
            'name'=>$payload['name'],
            'phone'=>$payload['phone']
        ]);
        $this->seeJsonDoesntContains([
            'data'=>[
                'password'
            ]
        ]);
    }

    public function testFailedUpdateAccountNoToken()
    {
        $url = '/accounts/'.$this->getAccount()->id;
        $payload = [
            'name'=>'Update',
            'phone'=>'54321'
        ];
        $this->json('PUT',$url,$payload);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false,
            'data'=>null
        ]);
    }

    public function testFailedGetOneAccountNotFound()
    {
        $url = '/accounts/'.$this->getAccount()->id+1;
        $payload = [
            'name'=>'Update',
            'phone'=>'54321'
        ];
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false,
            'data'=>null
        ]);       
    }

    public function testFailedUpdateAccountEmptyField()
    {
        $payload = [
            'username'=>'',
            'password'=>'',
            'name'=>'',
        ];
        $url = '/accounts/'.$this->getAccount()->id;
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                'username',
                'password',
                'name'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'username'=>['The username field is required.'],
                'password'=>['The password field is required.'],
                'name'=>['The name field is required.']
            ],
            'data'=>null
        ]);
    }

    public function testFailedUpdateAccountInvalidValue()
    {
        $payload = [
            'phone'=>'123123asd'
        ];
        $url = '/accounts/'.$this->getAccount()->id;
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                'phone'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'phone'=>['The phone must be a number.']
            ],
            'data'=>null
        ]);
    }

    public function testFailedUpdateAccountUsernameAlreadyExist()
    {
        $this->addAccount([]);
        $newAccount = $this->getAccount(1);
        $payload = ['username'=>$newAccount->username];
        $url = '/accounts/'.$this->getAccount()->id;
        $this->jsonUseJWT('PUT',$url,$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                'username'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'username'=>['The username has already been taken.']
            ],
            'data'=>null
        ]);
    }
}