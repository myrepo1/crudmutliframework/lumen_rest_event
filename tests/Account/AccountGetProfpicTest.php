<?php

class AccountGetProfpicTest extends TestCase
{
    public function testSuccessAccountGetPropfic(){
        $this->addAccount([],true);
        $url = $this->getAccount(1)->profpic;
        $this->get($url)->seeStatusCode(200);
    }

    public function testFailedAccountGetPropficNotFound(){
        $this->addAccount([],true);
        $url = $this->getAccount(1)->profpic.'a';
        $this->get($url)->seeStatusCode(404);
    }
}