<?php

class AccountCreateTest extends TestCase
{
    public function testSuccessCreateAccount()
    {
        $payload = [
            'username'=>'testing',
            'password'=>'testing',
            'name'=>'Testing',
            'phone'=>'123123'
        ];
        $this->json('POST','/accounts',$payload);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'data_id',
                'username',
                'name',
                'phone',
                'profpic',
                'is_staff'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>201,
            'status'=>true
        ]);
        $this->seeJsonDoesntContains([
            'data'=>[
                'password'
            ]
        ]);
        $this->assertEquals($payload['username'],$this->response['data']['username']);
        $this->assertFalse($this->response['data']['is_staff']);
    }

    public function testFailedCreateAccountEmptyField()
    {
        $payload = [
            'username'=>'',
            'password'=>'',
            'name'=>'',
            'phone'=>'123123'
        ];
        $this->json('POST','/accounts',$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                'username',
                'password',
                'name'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'username'=>['The username field is required.'],
                'password'=>['The password field is required.'],
                'name'=>['The name field is required.']
            ]
        ]);
        $this->assertNull($this->response['data']);
    }

    public function testFailedCreateAccountInvalidValue()
    {
        $payload = [
            'username'=>'testing',
            'password'=>'testing',
            'name'=>'Testing',
            'phone'=>'123123asd'
        ];
        $this->json('POST','/accounts',$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                'phone'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'phone'=>['The phone must be a number.']
            ]
        ]);
        $this->assertNull($this->response['data']);
    }

    public function testFailedCreateAccountUsernameAlreadyExist()
    {
        $payload = [
            'username'=>$this->getAccount()->username,
            'password'=>'testing',
            'name'=>'Testing',
            'phone'=>'123123'
        ];
        $this->json('POST','/accounts',$payload);
        $this->seeStatusCode(400);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'detail_message'=>[
                'username'
            ],
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>400,
            'status'=>false,
            'detail_message'=>[
                'username'=>['The username has already been taken.']
            ]
        ]);
        $this->assertNull($this->response['data']);
    }
}