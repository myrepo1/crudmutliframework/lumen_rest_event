<?php

class AccountAddProfpicTest extends TestCase
{
    public function testSuccessAccountAddProfpic()
    {
        $url = '/accounts/'.$this->getAccount()->id.'/image';
        $file = $this->getRandomImage();
        $this->uploadFileUseJWT($url,['profpic'=>$file]);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'data_id',
                'username',
                'name',
                'phone',
                'profpic',
                'is_staff'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true
        ]);
        $this->seeJsonDoesntContains(['password']);
        $this->assertNotNull($this->response['data']['profpic']);
        $this->assertTrue($this->checkURL($this->response['data']['profpic']));
    }

    public function testFailedAccountAddProfpicNoToken()
    {
        $url = '/accounts/'.$this->getAccount()->id.'/image';
        $this->uploadFile($url,['profpic'=>$this->getRandomImage()]);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false
        ]);
        $this->assertNull($this->response['data']);
    }

    public function testFailedAccountAddProfpicWrongId()
    {
        $this->addAccount(['is_staff'=>false]);
        $url = '/accounts/'.$this->getAccount()->id.'/image';
        $this->uploadFileUseJWT($url,['profpic'=>$this->getRandomImage()],[],[],'POST',1);
        $this->seeStatusCode(403);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>403,
            'status'=>false
        ]);
        $this->assertNull($this->response['data']);
    }
    
    public function testFailedAccountAddProfpicAccountNotFound()
    {
        
        $url = '/accounts/'.strval($this->getAccount()->id+1).'/image';
        $this->uploadFileUseJWT($url,['profpic'=>$this->getRandomImage()]);
        $this->seeStatusCode(404);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false
        ]);
        $this->assertNull($this->response['data']);
    }
}