<?php

class AccountGetAllTest extends TestCase {
    public function testSuccessGetAllAccount()
    {
        $this->jsonUseJWT('GET','/accounts');
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>['*'=>[
                'data_id',
                'username',
                'name',
                'phone',
                'profpic',
                'is_staff'
            ]]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true
        ]);
        $this->seeJson(['username'=>$this->getAccount()->username]);
        $this->seeJsonDoesntContains(['password']);
    }

    public function testFailedGetAllAccountNoToken()
    {
        $this->json('GET','/accounts');
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false,
            'data'=>null
        ]);
    }

    public function testFailedGetAllAccountWithNonStaff()
    {
        $this->addAccount(['is_staff'=>false]);
        $this->jsonUseJWT('GET','/accounts',[],[],1);
        $this->seeStatusCode(403);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>403,
            'status'=>false,
            'data'=>null
        ]);
    }
}