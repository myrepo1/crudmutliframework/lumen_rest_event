<?php

class AccountGetOneTest extends TestCase {
    public function testSuccessGetOneAccount()
    {
        $url = '/accounts/'.$this->getAccount()->id;
        $this->jsonUseJWT('GET',$url);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'=>[
                'data_id',
                'username',
                'name',
                'phone',
                'profpic',
                'is_staff'
            ]
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true,
            'username'=>$this->getAccount()->username
        ]);
        $this->seeJsonDoesntContains(['password']);
    }

    public function testFailedGetOneAccountNoToken()
    {
        $url = '/accounts/'.$this->getAccount()->id;
        $this->json('GET',$url);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false,
            'data'=>null
        ]);
    }

    public function testFailedGetOneAccountNotFound()
    {
        $url = '/accounts/'.$this->getAccount()->id+1;
        $this->jsonUseJWT('GET',$url);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
            'data'
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false,
            'data'=>null
        ]);       
    }
}