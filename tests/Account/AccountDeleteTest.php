<?php

class AccountDeleteTest extends TestCase
{
    public function testSuccessDeleteAccount()
    {
        $url = '/accounts/'.$this->getAccount()->id;
        $this->jsonUseJWT('DELETE',$url);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message',
        ]);
        $this->seeJsonContains([
            'status_code'=>200,
            'status'=>true
        ]);
    }

    public function testFailedDeleteAccountNoToken()
    {
        $url = '/accounts/'.$this->getAccount()->id;
        $this->json('DELETE',$url);
        $this->seeStatusCode(401);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message'
        ]);
        $this->seeJsonContains([
            'status_code'=>401,
            'status'=>false
        ]);
    }

    public function testFailedDeleteAccountNotFound()
    {
        $url = '/accounts/'.$this->getAccount()->id+1;
        $this->jsonUseJWT('DELETE',$url);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'status_code',
            'status',
            'message'
        ]);
        $this->seeJsonContains([
            'status_code'=>404,
            'status'=>false
        ]);       
    }
}