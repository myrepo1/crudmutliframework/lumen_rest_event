<?php

use Illuminate\Http\UploadedFile;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;

use App\Http\Helpers\Security;
use App\Models\Account;
use App\Models\Event;

use Dotenv\Dotenv;

abstract class TestCase extends BaseTestCase
{
    use DatabaseMigrations,Security;
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function setUp(): void
    {
        parent::setUp();
        Dotenv::createMutable(base_path(),'.env.testing')->load();
        $this->account = [$this->generateAccount()];
        $this->event = [$this->generateEvent()];
    }

    public function tearDown(): void
    {
        $test_dir = $this->app->storagePath('test');
        parent::tearDown();
        $this->deleteDirectory($test_dir);
    }

    public function getAccount($index=0)
    {
        return $this->account[$index];
    }

    public function getEvent($index=0)
    {
        return $this->event[$index];
    }

    public function addAccount($data,$use_profpic=false)
    {
        $account = $this->generateAccount($data);
        if($use_profpic){
            $file_info = $this->addFile('profpic');
            $url_filename = $file_info['name'].'__'.$file_info['ext'];
            $base_url = $this->app->environment('APP_URL');
            $account->profpic = $base_url.'/files/profpic/'.$url_filename;
            $account->save();
        }
        $this->account[] = $account;
    }

    public function addEvent($data,$use_preview=false)
    {
        $event = $this->generateEvent($data);
        if($use_preview){
            $file_info = $this->addFile('preview');
            $url_filename = $file_info['name'].'__'.$file_info['ext'];
            $base_url = $this->app->environment('APP_URL');
            $event->preview = $base_url.'/files/preview/'.$url_filename;
            $event->save();
        }
        $this->event[] = $event;
    }

    protected function generateAccount($data=[])
    {
        $account = Account::factory()->create($data);
        return $account;
    }

    protected function generateEvent($data=[])
    {
        $event = Event::factory()->create($data);
        return $event;
    }

    public function generateToken($account=0)
    {
        $account = $this->getAccount($account);
        $payload = [
            'id'=>$account->id,
            'is_staff'=>$account->is_staff
        ];
        $token = $this->generateJWT($payload);
        return $token;
    }

    protected function checkURL($url)
    {
        $result = filter_var($url,FILTER_VALIDATE_URL);
        if ($result===false){
            return false;
        } else {
            return true;
        }
    }

    protected function deleteDirectory($dirname) {
        $dir_handle = null;
        if (is_dir($dirname))
          $dir_handle = opendir($dirname);
        if (!$dir_handle)
            return false;
        while($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname."/".$file))
                        unlink($dirname."/".$file);
                else
                        $this->deleteDirectory($dirname.'/'.$file);
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

    protected function getRandomImage()
    {
        $file = UploadedFile::fake()->image('random.jpeg');
        return $file;
    }

    protected function addFile($dir){
        $test_dir = $this->app->storagePath('test');
        $source = $this->app->storagePath('static').'/'.$dir.'.jpeg';
        $filename = $dir;
        $ext = 'jpeg';
        $dest = $test_dir.'/'.$dir.'/'.$filename.'.'.$ext;
        if(!is_dir($test_dir.'/'.$dir)){
            mkdir($test_dir.'/'.$dir,0777,true);
        }
        copy($source,$dest);
        return ['container'=>$dir,'name'=>$filename,'ext'=>$ext];
    }

    protected function jsonUseJWT($method,$uri,$data=[],$headers=[],$account=0)
    {
        $token = $this->generateToken($account);
        $headers['Authorization'] = 'Bearer '.$token;
        return $this->json($method,$uri,$data,$headers);
    }

    protected function postUseJWT($uri,$data=[],$headers=[],$account=0)
    {
        $token = $this->generateToken($account);
        $headers['Authorization'] = 'Bearer '.$token;
        return $this->post($uri,$data,$headers);
    }

    protected function uploadFile($uri,$files=[],$headers=[],$data=[],$method='POST')
    {
        $server = $this->transformHeadersToServerVars($headers);
        return $this->call($method,$uri,$data,[],$files,$server);
    }

    protected function uploadFileUseJWT($uri,$files=[],$headers=[],$data=[],$method='POST',$account=0)
    {
        $token = $this->generateToken($account);
        $headers['Authorization'] = 'Bearer '.$token;
        return $this->uploadFile($uri,$files,$headers,$data,$method);
    }
}
